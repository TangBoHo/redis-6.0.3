#include "server.h"

/* This file implements keyspace events notification via Pub/Sub and
 * described at https://redis.io/topics/notifications. */

/* Turn a string representing notification classes into an integer
 * representing notification classes flags xored.
 *
 * The function returns -1 if the input contains characters not mapping to
 * any class. */
int keyspaceEventsStringToFlags(char *classes) {
    char *p = classes;
    int c, flags = 0;

    while((c = *p++) != '\0') {
        switch(c) {
            case 'A': flags |= NOTIFY_ALL; break;
            case 'g': flags |= NOTIFY_GENERIC; break;
            case '$': flags |= NOTIFY_STRING; break;
            case 'l': flags |= NOTIFY_LIST; break;
            case 's': flags |= NOTIFY_SET; break;
            case 'h': flags |= NOTIFY_HASH; break;
            case 'z': flags |= NOTIFY_ZSET; break;
            case 'x': flags |= NOTIFY_EXPIRED; break;
            case 'e': flags |= NOTIFY_EVICTED; break;
            case 'K': flags |= NOTIFY_KEYSPACE; break;
            case 'E': flags |= NOTIFY_KEYEVENT; break;
            case 't': flags |= NOTIFY_STREAM; break;
            case 'm': flags |= NOTIFY_KEY_MISS; break;
            default: return -1;
        }
    }
    return flags;
}

/* This function does exactly the revese of the function above: it gets
 * as input an integer with the xored flags and returns a string representing
 * the selected classes. The string returned is an sds string that needs to
 * be released with sdsfree(). */
sds keyspaceEventsFlagsToString(int flags) {
    sds res;

    res = sdsempty();
    if ((flags & NOTIFY_ALL) == NOTIFY_ALL) {
        res = sdscatlen(res,"A",1);
    } else {
        if (flags & NOTIFY_GENERIC) res = sdscatlen(res,"g",1);
        if (flags & NOTIFY_STRING) res = sdscatlen(res,"$",1);
        if (flags & NOTIFY_LIST) res = sdscatlen(res,"l",1);
        if (flags & NOTIFY_SET) res = sdscatlen(res,"s",1);
        if (flags & NOTIFY_HASH) res = sdscatlen(res,"h",1);
        if (flags & NOTIFY_ZSET) res = sdscatlen(res,"z",1);
        if (flags & NOTIFY_EXPIRED) res = sdscatlen(res,"x",1);
        if (flags & NOTIFY_EVICTED) res = sdscatlen(res,"e",1);
        if (flags & NOTIFY_STREAM) res = sdscatlen(res,"t",1);
    }
    if (flags & NOTIFY_KEYSPACE) res = sdscatlen(res,"K",1);
    if (flags & NOTIFY_KEYEVENT) res = sdscatlen(res,"E",1);
    if (flags & NOTIFY_KEY_MISS) res = sdscatlen(res,"m",1);
    return res;
}

/* The API provided to the rest of the Redis core is a simple function:
 *
 * notifyKeyspaceEvent(char *event, robj *key, int dbid);
 *
 * 'event' is a C string representing the event name.
 * 'key' is a Redis object representing the key name.
 * 'dbid' is the database ID where the key lives.  */
void notifyKeyspaceEvent(int type, char *event, robj *key, int dbid) {
    sds chan;
    robj *chanobj, *eventobj;
    int len = -1;
    char buf[24];

    /* If any modules are interested in events, notify the module system now.
     * This bypasses the notifications configuration, but the module engine
     * will only call event subscribers if the event type matches the types
     * they are interested in. */
    moduleNotifyKeyspaceEvent(type, event, key, dbid);

    /* If notifications for this class of events are off, return ASAP. */
    if (!(server.notify_keyspace_events & type)) return;

    eventobj = createStringObject(event,strlen(event));

    /* __keyspace@<db>__:<key> <event> notifications. */
    if (server.notify_keyspace_events & NOTIFY_KEYSPACE) {
        chan = sdsnewlen("__keyspace@",11);
        len = ll2string(buf,sizeof(buf),dbid);
        chan = sdscatlen(chan, buf, len);
        chan = sdscatlen(chan, "__:", 3);
        chan = sdscatsds(chan, key->ptr);
        chanobj = createObject(OBJ_STRING, chan);
        pubsubPublishMessage(chanobj, eventobj);
        decrRefCount(chanobj);
    }

    /* __keyevent@<db>__:<event> <key> notifications. */
    if (server.notify_keyspace_events & NOTIFY_KEYEVENT) {
        chan = sdsnewlen("__keyevent@",11);
        if (len == -1) len = ll2string(buf,sizeof(buf),dbid);
        chan = sdscatlen(chan, buf, len);
        chan = sdscatlen(chan, "__:", 3);
        chan = sdscatsds(chan, eventobj->ptr);
        chanobj = createObject(OBJ_STRING, chan);
        pubsubPublishMessage(chanobj, key);
        decrRefCount(chanobj);
    }
    decrRefCount(eventobj);
}



/*
PUBLISH __keyspace@0__:mykey del
PUBLISH __keyevent@0__:del mykey
订阅第一个频道 __keyspace@0__:mykey 可以接收 0 号数据库中所有修改键 mykey 的事件，
 而订阅第二个频道 __keyevent@0__:del 则可以接收 0 号数据库中所有执行 del 命令的键。
以 keyspace 为前缀的频道被称为键空间通知（key-space notification），
 而以 keyevent 为前缀的频道则被称为键事件通知（key-event notification）。
当 del mykey 命令执行时：
键空间频道的订阅者将接收到被执行的事件的名字，在这个例子中，就是 del 。
键事件频道的订阅者将接收到被执行事件的键的名字，在这个例子中，就是 mykey */
void notifySpecialKeyEvent(robj *key, int type) {
    sds chan;
    robj *chanobj, *eventobj;
    int vlen;
    sds *v = sdssplitlen(key->ptr,sdslen(key->ptr), APP_KEY_SPIT,1, &vlen);
    chan = type == NOTIFY_HOT ? sdsnewlen("_keyhot:",8) : sdsnewlen("_keydel:",8);
    chan = sdscatsds(chan, v[0]);
    chanobj = createObject(OBJ_STRING, chan);
    eventobj = createStringObject(v[1],strlen(v[1]));
    pubsubPublishMessage(chanobj, eventobj);
    decrRefCount(chanobj);
    decrRefCount(eventobj);

}



